APP_VERS = "1.0.3"

QT += core gui widgets network help charts sql svg xml printsupport

TARGET = ubpm

INCLUDEPATH += ../

SOURCES	+= MainWindow.cpp DialogAnalysis.cpp DialogHelp.cpp DialogRecord.cpp DialogSettings.cpp DialogUpdate.cpp
HEADERS	+= MainWindow.h   DialogAnalysis.h   DialogHelp.h   DialogRecord.h   DialogSettings.h   DialogUpdate.h
FORMS	+= MainWindow.ui  DialogAnalysis.ui  DialogHelp.ui  DialogRecord.ui  DialogSettings.ui  DialogUpdate.ui

RESOURCES		+= res/ubpm.qrc
TRANSLATIONS	+= lng/ubpm_de.ts lng/ubpm_ru.ts lng/ubpm_nl.ts

VERSION = $${APP_VERS}

unix:!macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	QMAKE_LFLAGS += -Wl,-rpath=.

	INSTALLDIR = /tmp/ubpm.AppDir

	!system(which linuxdeployqt-continuous-x86_64.AppImage >/dev/null) { warning("linuxdeployqt (https://github.com/probonopd/linuxdeployqt/releases) missing, cant't build package!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.so) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	exists($$INSTALLDIR) { system(rm -r $$INSTALLDIR) }

	target.path = $$INSTALLDIR/usr/bin

	translations.path  = $$INSTALLDIR/usr/bin/Languages
	translations.files = lng/ubpm_*.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_de.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_ru.qm

	themes.path  = $$INSTALLDIR/usr/bin/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/usr/bin/Plugins
	devices.files = ../plugins/*.so

	help.path  = $$INSTALLDIR/usr/bin/Guides
	help.files = hlp/*.qch hlp/*.qhc

	lin.path  = $$INSTALLDIR
	lin.extra = cp ../../package/lin/ubpm.desktop $$INSTALLDIR/ubpm.desktop &&\
				cp res/ico/app.png $$INSTALLDIR/ubpm.png &&\
				mkdir -p $$INSTALLDIR/usr/share/metainfo && cp ../../package/lin/de.lazyt.ubpm.appdata.xml $$INSTALLDIR/usr/share/metainfo/ &&\
				ln -s ubpm.png $$INSTALLDIR/.DirIcon &&\
				export VERSION=$$VERSION &&\
				mkdir $$INSTALLDIR/usr/lib && cp $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/binary/lib/libssl.so.1.1 $$INSTALLDIR/usr/lib && cp $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/binary/lib/libcrypto.so.1.1 $$INSTALLDIR/usr/lib &&\
				linuxdeployqt-continuous-x86_64.AppImage $$INSTALLDIR/ubpm.desktop -appimage -no-copy-copyright-files -no-translations &&\
				mv Universal_Blood_Pressure_Manager-*-x86_64.AppImage ubpm-"$$VERSION".AppImage

	INSTALLS += target translations themes devices help lin
}

win32 {
	APP_DATE = "$$system(date /t)"

	RC_ICONS = res/ico/app.ico
	RC_LANG  = "0x0407"

	QMAKE_TARGET_COMPANY     = "LazyT"
	QMAKE_TARGET_DESCRIPTION = "$${APP_NAME}"
	QMAKE_TARGET_COPYRIGHT   = "Thomas L\\366we, 2020-2021"
	QMAKE_TARGET_PRODUCT     = "UBPM"

	INSTALLDIR = $$(TMP)/ubpm.7zip

	!system(7z.exe >nul 2>&1) { warning("7zip (https://www.7-zip.org/download.html) missing, cant't build package!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlcipher.dll) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	!exists($$(SYSTEMROOT)/System32/msvcr100.dll) { warning("MSVCR missing, TLS via Qt OpenSSL not available!") }
	exists($$INSTALLDIR) { system(rmdir /s /q $$shell_path($$INSTALLDIR)) }
	exists($$(TMP)/ubpm.7z) { system(del $$shell_path($$(TMP)/ubpm.7z)) }

	target.path = $$INSTALLDIR

	translations.path  = $$INSTALLDIR/Languages
	translations.files = lng/ubpm_*.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_de.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_ru.qm

	themes.path  = $$INSTALLDIR/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/Plugins
	devices.files = ../plugins/*.dll

	help.path  = $$INSTALLDIR/Guides
	help.files = hlp/*.qch hlp/*.qhc

	libraries.path  = $$INSTALLDIR
	libraries.files = $$[QT_INSTALL_LIBEXECS]/Qt5Core.dll $$[QT_INSTALL_LIBEXECS]/Qt5Gui.dll $$[QT_INSTALL_LIBEXECS]/Qt5Widgets.dll  $$[QT_INSTALL_LIBEXECS]/Qt5Charts.dll $$[QT_INSTALL_LIBEXECS]/Qt5Network.dll $$[QT_INSTALL_LIBEXECS]/Qt5Help.dll $$[QT_INSTALL_LIBEXECS]/Qt5Sql.dll $$[QT_INSTALL_LIBEXECS]/Qt5Printsupport.dll  $$[QT_INSTALL_LIBEXECS]/Qt5Svg.dll $$[QT_INSTALL_LIBEXECS]/libgcc_s_seh-1.dll $$[QT_INSTALL_LIBEXECS]/libstdc++-6.dll $$[QT_INSTALL_LIBEXECS]/libwinpthread-1.dll $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/bin/libcrypto-1_1-x64.dll $$[QT_INSTALL_PREFIX]/../../Tools/OpenSSL/Win_x64/bin/libssl-1_1-x64.dll $$(SYSTEMROOT)/System32/msvcr100.dll 

	plugins1.path  = $$INSTALLDIR/platforms
	plugins1.files = $$[QT_INSTALL_PLUGINS]/platforms/qwindows.dll
	plugins2.path  = $$INSTALLDIR/styles
	plugins2.files = $$[QT_INSTALL_PLUGINS]/styles/qwindowsvistastyle.dll
	plugins3.path  = $$INSTALLDIR/imageformats
	plugins3.files = $$[QT_INSTALL_PLUGINS]/imageformats/qsvg.dll
	plugins4.path  = $$INSTALLDIR/sqldrivers
	plugins4.files = $$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlite.dll $$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlcipher.dll
	plugins5.path  = $$INSTALLDIR/printsupport
	plugins5.files = $$[QT_INSTALL_PLUGINS]/printsupport/windowsprintersupport.dll
 
	win.path  = $$INSTALLDIR
	win.extra = 7z a -mx9 $$(TMP)\ubpm.7z $$INSTALLDIR\* &&\
				copy /b ..\..\package\win\7zs2.sfx + $$(TMP)\ubpm.7z ubpm-"$$VERSION".exe

	INSTALLS += target translations themes devices help libraries plugins1 plugins2 plugins3 plugins4 plugins5 win
}

macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	ICON = res/ico/app.icns

	QMAKE_INFO_PLIST = ../../package/mac/Info.plist

	INSTALLDIR = $$(TMPDIR)/ubpm

	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.dylib) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }
	exists($$INSTALLDIR) { system(rm -r $$INSTALLDIR) }

	target.path = $$INSTALLDIR

	translations1.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Languages
	translations1.files = lng/ubpm_*.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_de.qm $$[QT_INSTALL_TRANSLATIONS]/qtbase_ru.qm
	translations2.path  = $$INSTALLDIR/ubpm.app/Contents
	translations2.files = ../../package/mac/Resources

	themes.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Themes
	themes.files = qss/*.qss

	devices.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Plugins
	devices.files = ../plugins/*.dylib

	help.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Guides
	help.files = hlp/*.qch hlp/*.qhc

	mac.path  = $$INSTALLDIR/ubpm.app
	mac.extra = cp -R ../../package/mac/Resources $$INSTALLDIR/ubpm.app/Contents &&\
				cd $$INSTALLDIR &&\
				ln -s /Applications &&\
				$$[QT_INSTALL_BINS]/macdeployqt ubpm.app &&\
				hdiutil create -volname UBPM -srcfolder $$INSTALLDIR $$PWD/ubpm-"$$VERSION".dmg

	INSTALLS += target translations1 translations2 themes devices help mac
}

DEFINES += APPVERS=\"\\\"$${APP_VERS}\\\"\" APPDATE=\"\\\"$${APP_DATE}\\\"\"
