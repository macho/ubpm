#include "DialogUpdate.h"

DialogUpdate::DialogUpdate(QWidget *pparent, bool pnotification) : QDialog(pparent)
{
	setupUi(this);

	groupBox_download->hide();

	parent = pparent;
	notification = pnotification;

	nam = new QNetworkAccessManager(this);

	nam->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
	nam->setTransferTimeout(TIMEOUT);

	connect(nam, &QNetworkAccessManager::sslErrors, this, &DialogUpdate::sslErrors);
	connect(nam, &QNetworkAccessManager::finished, this, &DialogUpdate::finished);

	nam->get(QNetworkRequest(RELEASES));

	return;
}

void DialogUpdate::sslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
	if(notification)
	{
		static bool ignore = false;
		int n = 0;
		QString msg;

		foreach(QSslError error, errors)
		{
			n++;

			msg.append("   - " + error.errorString() + "\n");
		}

		if(ignore || QMessageBox::question(parent, APPNAME, tr("!!! SSL WARNING - READ CAREFULLY !!!\n\nNetwork connection problem(s):\n\n%1\nDo you wish to continue anyway?", "", n).arg(msg), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
		{
			ignore = true;

			reply->ignoreSslErrors();
		}
	}
}

void DialogUpdate::finished(QNetworkReply *reply)
{
	bool download = reply->url().toString().contains("attachments");

	if(reply->error())
	{
		if(notification)
		{
			QMessageBox::warning(parent, APPNAME, download ? tr("Downloading update failed!\n\n%1").arg(reply->errorString()) : tr("Checking update failed!\n\n%1").arg(reply->errorString()));

			reject();
		}
	}
	else
	{
		download ? saveDownload(reply->readAll()) : checkRelease(reply->readAll());
	}
}

void DialogUpdate::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if(bytesTotal == -1)
	{
		bytesTotal = size;
	}

	if(bytesReceived && bytesTotal)
	{
		progressBar->setFormat(QString("%p% [ %L1 / %L2 Byte ]").arg(bytesReceived).arg(bytesTotal));

		progressBar->setValue(static_cast<int>((100 * bytesReceived) / bytesTotal));
	}
}

void DialogUpdate::checkRelease(QByteArray ba)
{
	QJsonObject jo = QJsonDocument::fromJson(ba).array().at(0).toObject();
	QString body, date, version;

	if(jo.contains("body"))
	{
		body = jo.value("body").toString();
	}

	if(jo.contains("published_at"))
	{
		date = jo.value("published_at").toString();
	}

	if(jo.contains("tag_name"))
	{
		version = jo.value("tag_name").toString();
	}

	if(jo.contains("assets"))
	{
		QJsonArray arr = jo.value("assets").toArray();

		for(int i = 0; i < arr.count(); i++)
		{
			if(arr[i].toObject().contains("browser_download_url"))
			{
				url = arr[i].toObject().value("browser_download_url").toString();
			}

			if(arr[i].toObject().contains("name"))
			{
				name = arr[i].toObject().value("name").toString();
			}

			if(arr[i].toObject().contains("size"))
			{
				size = arr[i].toObject().value("size").toInt();
			}

			if(name.endsWith(BIN))
			{
				break;
			}
		}
	}

	if(body.isEmpty() || date.isEmpty() || url.isEmpty() || name.isEmpty())
	{
		if(notification)
		{
			QMessageBox::warning(parent, APPNAME, tr("Unexpected response from update server!"));
		}

		close();
	}
	else
	{
		if(QVersionNumber::compare(QVersionNumber::fromString(version), QVersionNumber::fromString(APPVERS)) == 1)
		{
			label_icon->setPixmap(QIcon(IMG).pixmap(80, 80));

			label_installedVersion->setText(APPVERS);
			label_installedDate->setText(QString("[ %1 ]").arg(APPDATE));

			label_availableVersion->setText(version);
			label_availableDate->setText(QString("[ %1 ]").arg(QDateTime::fromString(date.left(10), "yyyy-MM-dd").toString("dd.MM.yyyy")));

			label_size->setText(QString("%L1 Bytes").arg(size));

			textEdit->setMarkdown(body);

			show();
		}
		else
		{
			if(notification)
			{
				QMessageBox::information(parent, APPNAME, tr("No new version found."));
			}

			close();
		}
	}
}

void DialogUpdate::saveDownload(QByteArray ba)
{
	QFile file(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation) + "/" + name);

	if(file.open(QIODevice::WriteOnly))
	{
		qint64 bytes = file.write(ba);
		file.setPermissions(file.permissions() | QFileDevice::ExeOwner | QFileDevice::ExeGroup | QFileDevice::ExeOther);
		file.close();

		if(bytes != size)
		{
			QMessageBox::warning(this, APPNAME, tr("Update doesn't have expected size!\n\n\%L1 : %L2\n\nRetry download…").arg(size).arg(bytes));
		}
		else
		{
			if(QMessageBox::question(this, APPNAME, tr("Update saved to %1.\n\nStart new version now?").arg(file.fileName()), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
			{
#if defined Q_OS_OSX
				if(QProcess::startDetached("open", QStringList(file.fileName())))
#else
				if(QProcess::startDetached(file.fileName(), QStringList()))
#endif
				{
					reinterpret_cast<MainWindow*>(parent)->forced = true;
					reinterpret_cast<MainWindow*>(parent)->close();
				}
				else
				{
					QMessageBox::warning(this, APPNAME, tr("Could not start new version!"));
				}
			}
		}
	}
	else
	{
		QMessageBox::warning(this, APPNAME, tr("Could not save update to %1!\n\n%2").arg(file.fileName(), file.errorString()));
	}

	close();
}

void DialogUpdate::on_pushButton_update_clicked()
{
	pushButton_update->setEnabled(false);

	groupBox_download->show();

	nr = nam->get(QNetworkRequest(url));

	connect(nr, &QNetworkReply::downloadProgress, this, &DialogUpdate::downloadProgress);
}

void DialogUpdate::on_pushButton_ignore_clicked()
{
	close();
}

void DialogUpdate::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		DialogHelp(reinterpret_cast<QWidget*>(parent), "01-06").exec();
	}

	QDialog::keyPressEvent(ke);
}

void DialogUpdate::reject()
{
	if(nr && nr->isRunning())
	{
		if(QMessageBox::question(this, APPNAME, tr("Really abort download?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
		{
			nr->abort();
		}

		return;
	}

	QDialog::reject();
}
