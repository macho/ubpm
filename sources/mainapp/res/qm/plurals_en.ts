<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DialogAnalysis</name>
    <message numerus="yes">
        <location filename="../../DialogAnalysis.cpp" line="211"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Result</numerusform>
            <numerusform>Results</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../DialogAnalysis.cpp" line="211"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Match</numerusform>
            <numerusform>Matches</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../DialogAnalysis.cpp" line="211"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Record</numerusform>
            <numerusform>Records</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message numerus="yes">
        <location filename="../../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem:

%1
Do you wish to continue anyway?</numerusform>
            <numerusform>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problems:

%1
Do you wish to continue anyway?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message numerus="yes">
        <location filename="../../MainWindow.cpp" line="2192"/>
        <location filename="../../MainWindow.cpp" line="2959"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Successfully imported %n record from %1.

     User 1 : %2
     User 2 : %3</numerusform>
            <numerusform>Successfully imported %n records from %1.

     User 1 : %2
     User 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../MainWindow.cpp" line="2196"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Skipped %n invalid record!</numerusform>
            <numerusform>Skipped %n invalid records!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../MainWindow.cpp" line="2201"/>
        <location filename="../../MainWindow.cpp" line="2963"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Skipped %n duplicate record!</numerusform>
            <numerusform>Skipped %n duplicate records!</numerusform>
        </translation>
    </message>
</context>
</TS>
