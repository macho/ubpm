<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importeren van apparaat</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Apparaat info</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="53"/>
        <source>Serial</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="60"/>
        <source>Producer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="67"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="116"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="133"/>
        <source>Write Logfile</source>
        <translation>Logfile schrijven</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="159"/>
        <source>Cancel</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Laden van de extensie &quot;vertaling&quot; is mislukt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="166"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Kan Logfile %1 niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="191"/>
        <source>Import aborted by user!</source>
        <translation>Importeren door gebruiker afgebroken!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="44"/>
        <source>Could not open usb device %1:%2!</source>
        <translation>Kan USB-apparaat %1:%2 niet openen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="176"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Druk op START / STOP op het apparaat en probeer het opnieuw …</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="233"/>
        <source>Import in progress!</source>
        <translation>Importeert!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="222"/>
        <source>Really abort import?</source>
        <translation>Importeren echt afbreken?</translation>
    </message>
</context>
</TS>
