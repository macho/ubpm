TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
INCLUDEPATH	+= ../../../../ ../../../shared/hidapi
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/hem-7080it.qrc
TRANSLATIONS	= res/qm/hem-7080it-de_DE.ts res/qm/hem-7080it-nl.ts
TARGET		= ../../../omron-hem7080it

unix:!macx {
SOURCES		+= ../../../shared/hidapi/hidlin.c
LIBS		+= -ludev
}

win32 {
SOURCES		+= ../../../shared/hidapi/hidwin.c
LIBS		+= -lsetupapi
CONFIG		-= debug_and_release
}

macx {
SOURCES		+= ../../../shared/hidapi/hidmac.c
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../shared/plugin/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
